# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://rokkondic@bitbucket.org/rokkondic/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/rokkondic/stroboskop/commits/ca464ef7596fa94411ce8e8ad8757ab5e9015ea0

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/rokkondic/stroboskop/commits/0d90485a4195172e2260ad5a5319490760a379d0

Naloga 6.3.2:
https://bitbucket.org/rokkondic/stroboskop/commits/e27426a6cac141f665393be34ed715e7c8bfc9a7

Naloga 6.3.3:
https://bitbucket.org/rokkondic/stroboskop/commits/8360a20f43b013857d707c5a5cf61bad0430e6a4

Naloga 6.3.4:
https://bitbucket.org/rokkondic/stroboskop/commits/2377830d0dea45f493ff75171d73a40e185e28e6

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/rokkondic/stroboskop/commits/ae905710fbfe5f7501042487869af08a75f08d90

Naloga 6.4.2:
https://bitbucket.org/rokkondic/stroboskop/commits/c9a5acb5daaa7f2af1d8b6b5560d1d3e911d6219

Naloga 6.4.3:
https://bitbucket.org/rokkondic/stroboskop/commits/85175034ccec34bd0bef809b3480a2ad0768179a

Naloga 6.4.4:
https://bitbucket.org/rokkondic/stroboskop/commits/e327e68544b77afea48e687c17a3c9c9afa02e70